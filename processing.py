import cv2
import numpy as np
from typing import Dict, Set, Tuple, Any, List
from statistics import mean

board_heigh = 6
board_width = 7

DEBUG = True

canny_low_threshold = 50
canny_high_threshold = 150
canny_aperture_size = 3

hough_rho = 1
hough_theta = np.pi / 180
hough_points_threshold = 120

Lmin_white = 150
Hmin_red = 160
Hmax_red = 20

def canny(img):
    return cv2.Canny(img, threshold1=canny_low_threshold, threshold2=canny_high_threshold, apertureSize=canny_aperture_size)

def hough_line_transform(img, edges) -> Tuple[Any, Dict[str, Set[Tuple[float, float, float, float]]]]:
    """
    :param img: imagen binarizada
    :param edges: lista de bordes obtenidos tras un método como Canny
    :return: result_image: image with lines drawn
             result_lines: dictionary with keys 'h' (horizontal) and 'v' (vertical), where there are stored lines
                            by storing two points of the line (x1, y1, x2, y2)
    """
    global DEBUG

    if DEBUG:
        cv2.imshow("x", img)
        cv2.imshow("canny", edges)

    lines = cv2.HoughLines(edges, hough_rho, hough_theta, hough_points_threshold)
    # Result image has 3 channels instead 1, we want to draw the lines in non-gray colors.
    result_image = cv2.merge([img, img, img])
    result_lines = {'h': set(), 'v': set()}

    if lines is not None:
        # We keep rho and theta values of lines used in order to not use other lines on the same edge
        used_lines = set()
        for line in lines:
            rho = line[0][0]
            theta = line[0][1]
            # Check if the line is similar to another one already drawn
            if any([abs(used_theta - theta) <= 1.4 and abs(used_rho - rho) <= 100 for used_rho, used_theta in used_lines]):
                continue
            # From polar coordinates to two points of the line (line is perpendicular to the vector with polar coords)
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * rho
            y0 = b * rho
            x1 = int(x0 + 1000 * (-b))
            y1 = int(y0 + 1000 * a)
            x2 = int(x0 - 1000 * (-b))
            y2 = int(y0 - 1000 * a)
            # We check if the line is horizontal (h), vertical (v) or oblique (discarded), then we store two points
            if abs(theta) <= 0.4 or abs(np.pi - theta) <= 0.4:
                result_lines['v'].add((x1, y1, x2, y2))
            elif abs(np.pi/2 - theta) <= 0.4:
                result_lines['h'].add((x1, y1, x2, y2))
            else:
                continue
            used_lines.add((rho, theta))
            cv2.line(result_image, (x1, y1), (x2, y2), (0, 0, 255), 3, cv2.LINE_AA)

    if DEBUG:
        cv2.imshow("hough", result_image)

    return result_image, result_lines


def corner_points(lines: Dict[str, Set[Tuple[float, float, float, float]]]) -> List[Tuple[int, int]]:
    '''
    :param lines:
    :return:
    '''
    def findIntersection(x1, y1, x2, y2, x3, y3, x4, y4):
        # https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
        px = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / (
                (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
        py = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / (
                (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
        return px, py

    # Intersection between horizontal lines and vertical lines
    intersection_points = [findIntersection(x1, y1, x2, y2, x3, y3, x4, y4)
                           for (x1, y1, x2, y2) in lines['h']
                           for (x3, y3, x4, y4) in lines['v']]

    if len(intersection_points) < 4:
        return None

    # mean is useful for finding corner points
    mean_x = mean([x for x, y in intersection_points])
    mean_y = mean([y for x, y in intersection_points])

    # Top left point will be x < mean_x and y < mean_y, also if there were more horizontal lines by mistake, we keep
    # the intersection with lower y (more at the top of the image)
    top_left_points = [(x, y) for (x, y) in intersection_points if x <= mean_x and y <= mean_y]
    if len(top_left_points) == 0: return None
    top_left = min(top_left_points, key=lambda p: p[1])

    top_right_points = [(x, y) for (x, y) in intersection_points if x >= mean_x and y <= mean_y]
    if len(top_right_points) == 0: return None
    top_right = min(top_right_points, key=lambda p: p[1])

    bottom_left_points = [(x, y) for (x, y) in intersection_points if x <= mean_x and y >= mean_y]
    if len(bottom_left_points) == 0: return None
    bottom_left = max(bottom_left_points, key=lambda p: p[1])

    bottom_right_points = [(x, y) for (x, y) in intersection_points if x >= mean_x and y >= mean_y]
    if len(bottom_right_points) == 0: return None
    bottom_right = max(bottom_right_points, key=lambda p: p[1])

    return [top_left, top_right, bottom_left, bottom_right]


def perspective_transformation(img, object_corner_points: List[Tuple[int, int]], frame_size: Tuple[int, int]):
    '''
    :param img:
    :param object_corner_points:
    :param frame_size:
    :return:
    '''
    object_corner_points = np.float32(object_corner_points)
    frame_corner_points = np.float32([(0, 0), (frame_size[0], 0), (0, frame_size[1]), frame_size])
    matrix = cv2.getPerspectiveTransform(object_corner_points, frame_corner_points)
    return cv2.warpPerspective(img, matrix, frame_size)


def perspective(hough_lines, img_to_transform):
    '''
    :param hough_lines:
    :param img_to_transform:
    :return:
    '''
    if len(hough_lines['h']) > 0 and len(hough_lines['v']) > 0:
        points = corner_points(hough_lines)
        if points is not None:
            shape = img_to_transform.shape
            after_perspective = perspective_transformation(img_to_transform, points, (shape[1], shape[0]))
            return after_perspective
    return None


def board_state_color_matrix(img):
    '''
    :param img:
    :return:
    '''
    global board_heigh, board_width

    heigh, width, _ = img.shape
    num_pixels_row = heigh // board_heigh
    num_pixels_colum = width // board_width

    res = np.zeros((board_heigh,board_width,3), dtype=np.uint8)

    aux_h = 0
    aux_row = 0
    while (aux_h+num_pixels_row)<=heigh:
        sr = aux_h+num_pixels_row
        aux_w = 0
        aux_colum = 0
        while (aux_w+num_pixels_colum)<=width:
            sc = aux_w + num_pixels_colum
            cell = img[aux_h:sr, aux_w:sc]

            central_pixel = [cell[cell.shape[0]//2 + i, cell.shape[1]//2 + j] for i, j in {(0, 0), (20, 0), (-20, 0), (0, 20), (0, -20)}]
            res[aux_row, aux_colum] = np.mean(central_pixel, 0)

            aux_w+=num_pixels_colum
            aux_colum += 1

        aux_h += num_pixels_row
        aux_row += 1

    return res


def color_classifier_HSV(bgr_board):
    '''
    :param bgr_board:
    :return:
    '''
    def get_color(pixel_HSV, pixel_HLS):
        h,s,v = pixel_HSV
        _, l, _ = pixel_HLS
        if l >= Lmin_white:
            return 1 # white
        if h<=Hmax_red or h>=Hmin_red:
            return 2 # red
        return 0 # other (empty cell)

    heigh,width,_ = bgr_board.shape
    res = np.zeros((heigh,width))
    board_HLS = cv2.cvtColor(bgr_board, cv2.COLOR_BGR2HLS)
    board_HSV = cv2.cvtColor(bgr_board, cv2.COLOR_BGR2HSV)
    for r in range(heigh):
        for c in range(width):
            res[r, c] = get_color(board_HSV[r, c], board_HLS[r, c])
    
    return res


def board_state(img):
    '''
    :param img:
    :return:
    '''
    global DEBUG
    board_bgr = board_state_color_matrix(img)
    clas = color_classifier_HSV(board_bgr)
    board_token_colors = debug_board(board_bgr, img.shape, False)
    board_state = debug_board(clas, img.shape, True)
    if DEBUG:
        cv2.imshow("processed image",  board_token_colors)
        cv2.imshow("colours clasificated",  board_state)
    return board_token_colors, board_state, clas


def debug_board(board, shape, using_hsv_classifier):
    '''
    :param board:
    :param shape:
    :param using_hsv_classifier:
    :return:
    '''
    def get_color(n):
        if n==1: # white
            return np.array([255,255,255])
        if n==2: # red
            return np.array([0,0,255])
        return np.array([0,0,0]) 
    
    heigh, width, _ = shape
    num_pixels_row = heigh // board.shape[0]
    num_pixels_colum = width // board.shape[1]

    res = np.full(shape, [255,0,0], np.uint8)

    aux_h = 0
    aux_row = 0
    while (aux_h+num_pixels_row)<=heigh:
        sr = aux_h+num_pixels_row
        aux_w = 0
        aux_colum = 0
        while (aux_w+num_pixels_colum)<=width:
            sc = aux_w + num_pixels_colum

            color = board[aux_row, aux_colum]
            if using_hsv_classifier:
                color = get_color(color)
            
            center = (num_pixels_colum//2, num_pixels_row//2)
            color = (int(color[0]), int(color[1]), int(color[2]))        
            cv2.circle(res[aux_h:sr, aux_w:sc], center, int(num_pixels_row*0.4), color, -1)
           
            aux_w+=num_pixels_colum
            aux_colum += 1

        aux_h += num_pixels_row
        aux_row += 1

    return res
