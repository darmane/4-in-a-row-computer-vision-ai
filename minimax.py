import numpy as np


#####################################################

# global variables
row_length = 4
token_player_max = 1
token_player_min = 2
difficulty = 3

board_heigh = 6
board_width = 7

#####################################################


def token_in_bounds(x, y, height, width):
    """
        Checks if the current token is within the limits of the board
    """
    return (x>=0 and x<height) and (y>=0 and y<width)


def match_n_in_a_row(state, x, y, token):
    """
        checks if there is a n-connection by checking the n-1 neighbors of a token in all directions:
            (-1,-1), (-1,0), (-1,1), (0,-1), (0,1), (1,-1), (1,0), (1,1)
    """
    global row_length

    height, width = state.shape
    for dx in [-1,0,1]:
        for dy in [-1,0,1]:
            if not(dx==0 and dy==0):
                for n in range(1,row_length):
                    new_x = x + dx*n
                    new_y = y + dy*n
                    if not token_in_bounds(new_x, new_y, height, width):
                        break
                    else:
                        new_token = state[new_x][new_y]
                        if new_token!=token:
                            break
                        else:
                            if n==(row_length-1):
                                return True                                           
    return False 


def check_n_in_a_row_all_board(state):
    """
        checks if there is a n-connection in all the board by checking the n-1 neighbors of all tokens in all directions:
            (-1,-1), (-1,0), (-1,1), (0,-1), (0,1), (1,-1), (1,0), (1,1)
    """
    height, width = state.shape
    for x in range(height):
        for y in range(width):
            token = state[x][y]
            if token!=0 and match_n_in_a_row(state,x,y,token):
                return True, token
    return False, 0


def check_board_is_full(state):
    """
        checks if there is not any free places in the board
    """
    return not 0 in state


def game_ended(state):
    """
        Checks if the game is over.
        It is when:
            1. there is a n-connection.
            2. the are not free spaces in the board
    """
    return check_board_is_full(state) or check_n_in_a_row_all_board(state)[0]


def next_movement_minimax(state):
    """
        minimax algorith that determines the next movement of the AI (a colum where to place a token)
    """
    height, width = state.shape

    def put_token_in_colum(state, token,colum_index):
        """
            returns a new state of the board with the token placed
            on the specified  colum
        """
        for row in np.flip(range(height-1)):
            if state[row][colum_index] == 0:
                next_state = np.copy(state)
                next_state[row][colum_index] = token
                return next_state
    
    def colum_is_full(state, colum_index):
        """
            returns true if there are not any free spaces in the colum
        """
        return not 0 in state[:,colum_index]
    
    def evaluation(state):
        """
            returns an evaluation of the given state and the winner's token value.
            values:
                -> 1000 if wins max
                -> -1000 if wins min
                -> 0 if nobody wins
        """
        winner, winner_token = check_n_in_a_row_all_board(state)
        if winner:
            if winner_token == token_player_max:
                return 1000
            else:
                return -1000
        return 0 
    
    def minimax(state, depth, alpha, beta, player_max, colum):
        """
            Minimax Algorithm to play 4 in a row:
                - Case base: depth=0 or game is over (check game_ended method)
                - Recursion: alpha-beta pruning 
            Returns the score (evaluation) of the next state and the selected movement (next colum)
        """
        if depth == 0 or game_ended(state):          
            return evaluation(state), colum  
        else:
            if player_max:
                max_score = -10000000
                max_score_colum = colum
                for c in range(width):
                    if not colum_is_full(state, c):
                        next_state = put_token_in_colum(state, token_player_max, c)
                        new_score, _ = minimax(next_state, depth-1, alpha, beta, False, c)
                        if max_score < new_score:
                            max_score = new_score
                            max_score_colum = c
                        alpha = max(alpha, new_score)
                        if alpha >= beta:
                            break
                return max_score, max_score_colum
            else:
                min_score = 10000000
                min_score_colum = colum
                for c in range(width):
                    if not colum_is_full(state, c):
                        next_state = put_token_in_colum(state, token_player_min, c)
                        new_score, _  = minimax(next_state, depth-1, alpha, beta, True, c)
                        if min_score > new_score:
                            min_score = new_score
                            min_score_colum = c
                        beta = min(beta, new_score)
                        if alpha >= beta:
                            break
                return min_score, min_score_colum


    n_in_row, token_winner = check_n_in_a_row_all_board(state)
    if n_in_row:
        if token_winner==token_player_max:
            return "IA WINS"
        return "HUMAN WINS"
    elif check_board_is_full(state):
        return "DRAW"
    else:
        score, new_colum = minimax(state, difficulty, -10000000, 10000000 , True, None)
        return "New colum IA: " + str(new_colum+1)



# ############# DEBUG ###############

# def debug_four_in_row(number_of_boards):
#     """
#         tries the minimax algorithm n times with random boards
#     """
#     from random import randint

#     st = np.array([[0, 0, 0, 0, 0, 0, 0,], 
#             [0, 0, 0, 0, 0, 0, 0], 
#             [0, 0, 0, 0, 0, 0, 0],
#             [0, 1, 0, 2, 2, 0, 0], 
#             [1, 2, 0, 2, 1, 0, 0], 
#             [2, 2, 0, 2, 1, 1, 2]])

#     def create_new_game():
#         """
#             Creates a random state of the game
#         """
#         global board_heigh, board_width

#         board = np.zeros((board_heigh, board_width), dtype=int)
#         for i in range(board.shape[0]):
#             for j in range(board.shape[1]):
#                 if i==0:
#                     board[i][j] = randint(1,2)
#                 elif board[i-1][j]!=0:
#                     board[i][j] = randint(0,2)
#         return np.flip(board, 0)
    
#     for _ in range(number_of_boards):
#         #board = create_new_game()
#         board = st

#         print('Original')
#         print(board)
#         print("New state:\n", next_movement_minimax(board))

