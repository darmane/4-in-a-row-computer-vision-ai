import cv2
import numpy as np
import threshold_inRange as th
import webcam_image as wcam
import json

def set_threshold_webcam(webcam):
    """
        show the image from the webcam in b&w and allow
        the user to set the threshold with trackbars
    """
    # Load previous used hsv threshold values
    with open('./hsv_values.json', 'r') as f:
        try:
            all_values = json.load(f)
            if all_values['blue1']:
                th.low_H, th.low_S, th.low_V, th.high_H, th.high_S, th.high_V = all_values['blue1']
        except:
            pass

    th.create_trackbars_window()
    while True:
        if wcam.end_capture():
            #th.close_trackbars_window()
            cv2.destroyAllWindows()
            break
        frame = wcam.capture(webcam)
        bin_frame = binarize(frame)
        cv2.imshow(th.window_capture_name, frame)
        cv2.imshow(th.window_detection_name, bin_frame)

    # Stores current used hsv threshold values
    with open('./hsv_values.json', 'w') as f:
        json.dump({
            'blue1': (th.low_H, th.low_S, th.low_V, th.high_H, th.high_S, th.high_V)
        }, f)

def binarize(frame):
    """
        binarize the frame as parameter from HSV to black&white
        whit the current thresholds
    """
    frame_HSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    return cv2.inRange(frame_HSV, th.low_HSV(), th.high_HSV())