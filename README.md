# 4 in a Row Computer Vision AI

Play 4 in a row with the machine. Reads the state of a four-in-a-row board with the webcam in real life and a minimax algorithm determines the next move of the machine.

## Stack

- Python
- OpenCV

## License

MIT License.

You can fork this project for free under the following conditions:

- Add a link to this project.
- The software is provided on an "as is" basis and without warranty of any kind. Use it at your own risk.


**Requisitos**:

Python 3.x instalado.

Webcam conectada al ordenador.

Tablero físico de 4 en raya.

**Librerías**:

opencv-python

numpy

wxPython

**Instrucciones**

Al descomprimir el fichero .zip se obtendrá todo el proyecto. Para instalar las librerías debes navegar hasta la raíz del proyecto y escribir en la consola:

```pip install -r requirements.txt```

Si hay algún error en este procedimiento, se deberán instalar las librerías por separado.

Una vez instaladas las dependencias, bastará con hacer doble click en app.py para ejecutar la aplicación. En caso de que la webcam no se haya reconocido correctamente, se mostrará un mensaje de advertencia.

Lo primero que hay que hacer es seleccionar los umbrales HSV del color de nuestro tablero, para ello se mostrará la webcam y una binarización de la webcam en todo momento. Todas las pruebas se han realizado con un tablero azul pero cualquier color distinto de blanco o rojo sería válido.

Una vez seleccionados los umbrales correctos hay que pulsar la tecla “q” para ir a la interfaz principal.

Se puede ver la interfaz principal en la siguiente imagen:

Lo primero que se verá en la aplicación será un tutorial explicando su funcionamiento. Podremos modificar todos los parámetros para comprobar cómo afecta al resultado. Cualquier cambio que hagamos es volátil, se perderán al salir de la aplicación.
