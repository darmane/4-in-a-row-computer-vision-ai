##########################################################################################################
#
#   LEEME
#
# Este script permite crear un archivo ejecutable del proyecto.
# Se necesita instalar la dependencia "cx-freeze" para utilizarlo.
# OJO: nosotros hemos tenido algun que otro problema para instalar esta dependencia en Linux, ánimo.
##########################################################################################################



import sys
from cx_Freeze import setup, Executable

sys.argv.append('build')

setup(
    options= {'build_exe': {
        "include_files": ['hsv_values.json'],
    }},
    name="Transformada de Hough para 4 en raya",
    version="1",
    description="Transformada de Hough para 4 en raya",
    executables=[Executable("app.py", base=None)])

