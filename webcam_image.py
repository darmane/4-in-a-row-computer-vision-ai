import cv2


# Global Variables  
default_index = 0
window_capture_name = 'Video Capture'
window_object_detection_name = 'Object Detection'

# Methods

def webcam_available(webcam):
    """ 
        Checks if the webcam that you want to use is
        available or not. Returns True if it's available
        or False if not.
    """
    if webcam is None:
        return False
    return True

def set_webcam(index = default_index):
    """
        Returns a webcam of your system if it is available
    """
    new_webcam = cv2.VideoCapture(index)
    if webcam_available(new_webcam):
        new_webcam.open(index)
        return new_webcam
    else:
        print("\n\n*** ERROR ***\nUnable to use webcam with intex: ", index, "\n\n")
        return None
        
def capture(webcam):
    """
        Captures one frame from the webcam as parameter
    """
    _, frame = webcam.read()
    return frame

def end_capture():
    """
        Condition for end the video capture
    """
    if cv2.waitKey(1) & 0xFF == ord('q'):
        return True
    return False






